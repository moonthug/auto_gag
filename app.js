/**
 * Created by axc
 */

///////////////////////////////////////////////////////////////////////////////
//
//
// DEPENDENCIES
//

// Packages
var fs              = require('fs'),
    path            = require('path'),
    childProcess    = require('child_process');

// Libs
var async           = require('async'),
    cheerio         = require('cheerio'),
    request         = require('request');

///////////////////////////////////////////////////////////////////////////////
//
//
// IMPLEMENTATION
//

///////////////////////////////////////
//
// CONFIG


var users   = ['echo123'];
var topic   = 'Auto Gags';


///////////////////////////////////////
//
// METHODS

function sendSkypeMessage(message) {
    var chatId     = fs.readFileSync('skype_group.id').toString();

    var inputScript = fs.readFileSync('scripts/skype_input.script').toString();
    inputScript     = inputScript.replace(/\{\{CHAT_ID\}\}/g, chatId);
    inputScript     = inputScript.replace(/\{\{MESSAGE\}\}/g, message);

    fs.writeFileSync('skype_output.script', inputScript);

    var outputScriptPath = path.resolve('skype_output.script');

    var ch = childProcess.spawn('/usr/bin/osascript', [outputScriptPath]);
}

function getRandomJoke() {
    async.retry(5, requestJoke, function(err, result) {
        if(err) {
            console.log(' !!! Gag Failed!');
        }
        else {
            console.log(' --- Gag Sent!');

        }
        process.exit();
    });
}

function requestJoke(cb) {
    request('http://www.goodbadjokes.com/random', function(err, res, body) {
        if(err) return cb(new Error('Couldn\'t get a gag: ' + err.toString()));

        if(res.statusCode == 200) {
            var $ = cheerio.load(body);

            var question = $('.joke-content dt').text();
            var answer   = $('.joke-content dd').text();

            if(question == '' || answer == '') {
                return cb(new Error('Couldn\'t get a gag...'));
            }

            var message = question + '\n' + answer;

            console.log(' --- Sending gag: \n' + message);
            sendSkypeMessage(message);
            cb();
        }
    });
}

function generateGroupId(cb) {
    var inputScript = fs.readFileSync('scripts/skype_createchat.script').toString();
    inputScript     = inputScript.replace(/\{\{USERS\}\}/g, users.join(','));
    inputScript     = inputScript.replace(/\{\{TOPIC\}\}/g, topic);

    fs.writeFileSync('skype_createchat_output.script', inputScript);
    var outputScriptPath = path.resolve('skype_createchat_output.script');

    var output = '';
    var osascript = childProcess.spawn('/usr/bin/osascript', [outputScriptPath]);

    osascript.stdout.on('data', function (data) {
        output += data;
    });

    osascript.on('close', function (code) {
        fs.writeFileSync('skype_group.id', output.replace(/(\r\n|\n|\r)/gm,''));
        cb();
    });
}


///////////////////////////////////////
//
// MAIN

if(!fs.existsSync('skype_group.id')) {
    generateGroupId(getRandomJoke);
}
else {
    getRandomJoke();
}